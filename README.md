# About
As an amateur seamstress, I own quite a big fabric collection – pieces of textile, bought or inherited during the recent 30 years. The current database application is intended to help tracking my collection. The main database table contains information about fabrics - each piece has properties like colour, pattern, composition, length, width, price, weight etc. Photos can be added. The information about my sewing projects is stored in the second table. The third table keeps the data, relating the fabrics and sewing projects, i.e. which fabric(s) were used for particular work as well as the length(s) of used piece(s).
# Installation and Running Instructions

## DATABASE SETUP

##### Connect with the MySQL/MariaDB server
~~~
sudo mysql -u root -p
~~~

##### Create DB user
~~~
CREATE USER 'fabrics'@'%' IDENTIFIED BY 'tere';
~~~

##### Create DB
~~~
CREATE DATABASE fabrics CHARACTER SET UTF8MB4 COLLATE UTF8MB4_UNICODE_CI;
~~~

##### Grant privileges to the DB user
~~~
GRANT ALL PRIVILEGES ON fabrics.* TO 'fabrics'@'%';
~~~

## BUILD
Navigate to the project root folder and then...
~~~
./gradlew clean build (Linux)
~~~
...or...
~~~
gradlew clean build (Windows)
~~~


## RUN APPLICATION

Navigate to the project root folder and then...
~~~
./gradlew bootRun (Linux)
~~~
...or...
~~~
gradlew bootRun (Windows)
~~~
...or...
~~~
java -jar build/libs/fabrics-api.jar
~~~
