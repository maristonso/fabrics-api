-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fabrics
CREATE DATABASE IF NOT EXISTS `fabrics` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `fabrics`;

-- Dumping structure for table fabrics.fabric
CREATE TABLE IF NOT EXISTS `fabric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weaveType` varchar(50) DEFAULT NULL,
  `mainMaterial` varchar(50) DEFAULT NULL,
  `composition` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `pattern` varchar(50) DEFAULT NULL,
  `length` double DEFAULT NULL,
  `lengthRemained` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `unitWeight` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `isWashed` tinyint(4) DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `shop` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.fabric: ~7 rows (approximately)
/*!40000 ALTER TABLE `fabric` DISABLE KEYS */;
REPLACE INTO `fabric` (`id`, `weaveType`, `mainMaterial`, `composition`, `date`, `color`, `pattern`, `length`, `lengthRemained`, `width`, `weight`, `unitWeight`, `price`, `unitPrice`, `isWashed`, `comment`, `shop`, `url`) VALUES
	(1, NULL, 'wool', 'vill', '2020-02-06', 'Roosa', 'paisley', 5, 1.5, 1.4, 300, 42.857142857142854, 90, 18, 0, NULL, 'Solveig', 'http://localhost:8101/files/file/925fbbdd-ae14-4d4d-b0d9-8634a2ec8db5.jpg'),
	(2, NULL, 'silk', '100% silk', '2017-02-06', 'red', 'plaid', 3, 1, 2, 200, 33.333333333333336, 12, 4, 0, NULL, 'Solveig', 'http://localhost:8101/files/file/8deba77b-26a9-498d-b644-0e846692791d.jpg'),
	(3, NULL, 'cupro', 'cupro', '2010-03-06', 'blue', 'paisley', 4, 3, 1.1, 1000, 227.27272727272725, 10, 2.5, 0, NULL, 'supertkaniny', 'http://localhost:8101/files/file/c8644ae0-e7a5-4def-99d0-6a744d05d627.jpg'),
	(58, NULL, 'wool', 'vill', '2016-03-05', 'Roosa', 'tartan', 6, 6, 1.5, 1000, 111.11111111111111, 101, 16.833333333333332, 0, NULL, 'Saana', 'http://localhost:8101/files/file/a3b6358e-3595-4a9a-95e3-a0a3dc7c4681.jpg'),
	(67, NULL, 'cotton', 'puuvill', '2020-02-06', 'green', 'floral', 10, 0, 1.4, 1000, 71.42857142857143, 50, 5, 0, NULL, 'Abakhan', 'http://localhost:8101/files/file/9e0cbe7b-edc8-4904-a722-f8a8eb50bb0b.jpg'),
	(68, NULL, 'actate', 'actate', '2020-02-07', 'black', 'plain', 5, 4.7, 1.1, 800, 145.45454545454547, 70, 14, 0, NULL, 'mood fabrics', 'http://localhost:8101/files/file/e4e6279d-ab1a-4062-a28f-38699ab1f7f1.jpg'),
	(71, NULL, 'polyester', 'poly', '2020-02-12', 'red', 'plain', 3, 3, 1.5, 700, 155.55555555555554, 30, 10, 0, NULL, 'solveig', 'http://localhost:8101/files/file/674d9a77-b602-4b7f-995f-01d842e8ca0a.jpg');
/*!40000 ALTER TABLE `fabric` ENABLE KEYS */;

-- Dumping structure for table fabrics.piece
CREATE TABLE IF NOT EXISTS `piece` (
  `piece_id` int(11) NOT NULL AUTO_INCREMENT,
  `fabric_id` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  PRIMARY KEY (`piece_id`),
  KEY `FK1_fabric_id` (`fabric_id`),
  CONSTRAINT `FK1_fabric_id` FOREIGN KEY (`fabric_id`) REFERENCES `fabric` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.piece: ~0 rows (approximately)
/*!40000 ALTER TABLE `piece` DISABLE KEYS */;
/*!40000 ALTER TABLE `piece` ENABLE KEYS */;

-- Dumping structure for table fabrics.relation
CREATE TABLE IF NOT EXISTS `relation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `fabricId` int(11) DEFAULT NULL,
  `sewingId` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_relation_fabric` (`fabricId`),
  KEY `FK_relation_sewing` (`sewingId`),
  CONSTRAINT `FK_relation_fabric` FOREIGN KEY (`fabricId`) REFERENCES `fabric` (`id`),
  CONSTRAINT `FK_relation_sewing` FOREIGN KEY (`sewingId`) REFERENCES `sewing` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.relation: ~4 rows (approximately)
/*!40000 ALTER TABLE `relation` DISABLE KEYS */;
REPLACE INTO `relation` (`Id`, `fabricId`, `sewingId`, `length`) VALUES
	(27, 1, 1, 2),
	(29, 2, 1, 2),
	(30, 68, 1, 0.3),
	(31, 67, 5, 1.5),
	(32, 58, 14, 2),
	(33, 1, 8, 1.5),
	(34, 3, 14, 1);
/*!40000 ALTER TABLE `relation` ENABLE KEYS */;

-- Dumping structure for table fabrics.sewing
CREATE TABLE IF NOT EXISTS `sewing` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `explanation` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.sewing: ~7 rows (approximately)
/*!40000 ALTER TABLE `sewing` DISABLE KEYS */;
REPLACE INTO `sewing` (`Id`, `date`, `explanation`) VALUES
	(1, '2019-02-04', 'jacket'),
	(5, '2020-02-05', 'blouse'),
	(8, '2019-02-04', 'seelik'),
	(14, '2019-02-04', 'mantel'),
	(18, '2020-02-02', 'seelik, volditud'),
	(41, '2020-01-22', 'põll'),
	(42, '2020-02-07', 'asdfsdaf');
/*!40000 ALTER TABLE `sewing` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
