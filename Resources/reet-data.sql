-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fabrics
DROP DATABASE IF EXISTS `fabrics`;
CREATE DATABASE IF NOT EXISTS `fabrics` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `fabrics`;

-- Dumping structure for table fabrics.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `established` date DEFAULT NULL,
  `employees` int(11) DEFAULT NULL,
  `revenue` decimal(12,2) DEFAULT NULL,
  `net_income` decimal(12,2) DEFAULT NULL,
  `securities` int(11) DEFAULT NULL,
  `security_price` decimal(10,2) DEFAULT NULL,
  `dividends` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table fabrics.company: ~12 rows (approximately)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
REPLACE INTO `company` (`id`, `name`, `logo`, `established`, `employees`, `revenue`, `net_income`, `securities`, `security_price`, `dividends`) VALUES
	(1, 'Arco Vara', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC', '1994-07-04', 20, 3640000.00, -540000.00, 8998367, 1.09, 0.01),
	(2, 'Baltika', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT', '1997-05-09', 946, 44690000.00, -5120000.00, 4079485, 0.32, NULL),
	(3, 'Ekspress Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=EEG', '1995-06-21', 1698, 60490000.00, 10000.00, 29796841, 0.84, NULL),
	(4, 'Harju Elekter', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=HAE', '1996-05-14', 744, 120800000.00, 1550000.00, 17739880, 4.25, 0.18),
	(5, 'LHV Group', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV', '2005-01-25', 366, 64540000.00, 25240000.00, 26016485, 11.75, 0.21),
	(6, 'Merko Ehitus', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK', '1990-11-05', 740, 418010000.00, 19340000.00, 17700000, 9.30, 1.00),
	(7, 'Nordecon', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=NCN', '1998-01-01', 662, 223500000.00, 3380000.00, 32375483, 1.04, 0.12),
	(8, 'Pro Kapital Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=PKG', '1994-01-01', 91, 27990000.00, 16830000.00, 56687954, 1.43, NULL),
	(9, 'Tallink Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL', '1997-08-21', 7201, 949720000.00, 40050000.00, 669882040, 0.96, 0.12),
	(10, 'Tallinna Kaubamaja Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TKM', '1960-07-21', 4293, 681180000.00, 30440000.00, 40729200, 8.36, 0.71),
	(11, 'Tallinna Sadam', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM', '1991-12-25', 468, 130640000.00, 24420000.00, 263000000, 1.96, 0.13),
	(12, 'Tallinna Vesi', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TVE', '1967-01-01', 314, 62780000.00, 24150000.00, 20000000, 10.80, 0.75);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Dumping structure for table fabrics.fabric
DROP TABLE IF EXISTS `fabric`;
CREATE TABLE IF NOT EXISTS `fabric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weaveType` varchar(50) DEFAULT NULL,
  `mainMaterial` varchar(50) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `isWashed` tinyint(4) DEFAULT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `composition` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `pattern` varchar(50) DEFAULT NULL,
  `length` double DEFAULT NULL,
  `lengthRemained` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `unitWeight` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `unitPrice` double DEFAULT NULL,
  `shop` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.fabric: ~6 rows (approximately)
/*!40000 ALTER TABLE `fabric` DISABLE KEYS */;
REPLACE INTO `fabric` (`id`, `weaveType`, `mainMaterial`, `url`, `isWashed`, `comment`, `composition`, `date`, `color`, `pattern`, `length`, `lengthRemained`, `width`, `weight`, `unitWeight`, `price`, `unitPrice`, `shop`) VALUES
	(23, NULL, NULL, 'http://localhost:8101/files/file/c9b7c27e-ef77-4a99-b067-97fada88fa16.jpg', 0, NULL, 'bomull', '2020-02-06', 'valge', 'täpiline', 25, -1, 10, 20, 0.08, 150, 6, '"Tekstiil"'),
	(26, NULL, NULL, 'http://localhost:8101/files/file/cec9988a-d695-406a-840d-a8fb986b2256.jpg', 0, NULL, 'puuvillane', '2020-02-10', 'kollane', 'lilleline', 9, 0, 0, 5, 0, 85, 9.444444444444445, 'NBA'),
	(27, NULL, NULL, 'http://localhost:8101/files/file/7287d0b3-e658-40df-82a8-df672b5332ea.png', 0, NULL, 'siid', '2020-02-10', 'must-valge', 'täpiline', 8, 2, 1, 8, 1, 58, 7.25, 'ATM'),
	(30, NULL, NULL, 'http://localhost:8101/files/file/98a5b9c7-cfb5-4e30-abf4-76c4b3e5eb2f.jpg', 0, NULL, 'Siid', '2020-02-01', 'roheline', 'täpiline', 10, -4.5, 5, 2, 0.04, 25, 2.5, 'Kangapood'),
	(31, NULL, NULL, 'http://localhost:8101/files/file/83ef381f-cc1d-406c-b84c-b400c0d3c2bc.png', 0, NULL, 'mohäär', '2020-02-10', 'sinine', 'triibuline', 15, 0, 2, 4, 0.13333333333333333, 20, 1.3333333333333333, 'Nööp'),
	(33, NULL, NULL, 'http://localhost:8101/files/file/d3bb7eda-2d0c-44ed-84ce-3ee8746172a9.jpg', 0, NULL, 'siid', '2020-02-04', 'vikerkaar', 'vikerkaar', 10, 10, 20, 20, 0.1, 2, 0.2, 'Ebay');
/*!40000 ALTER TABLE `fabric` ENABLE KEYS */;

-- Dumping structure for table fabrics.relation
DROP TABLE IF EXISTS `relation`;
CREATE TABLE IF NOT EXISTS `relation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `sewingId` int(11) DEFAULT NULL,
  `fabricId` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK1_relation_fabric` (`fabricId`),
  KEY `FK2_relation_sewing` (`sewingId`),
  CONSTRAINT `FK1_relation_fabric` FOREIGN KEY (`fabricId`) REFERENCES `fabric` (`id`),
  CONSTRAINT `FK2_relation_sewing` FOREIGN KEY (`sewingId`) REFERENCES `sewing` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.relation: ~6 rows (approximately)
/*!40000 ALTER TABLE `relation` DISABLE KEYS */;
REPLACE INTO `relation` (`Id`, `sewingId`, `fabricId`, `length`) VALUES
	(16, 3, 30, 2),
	(19, 19, 30, 2.5),
	(20, 22, 27, 2),
	(22, 19, 26, 0.5),
	(23, 21, 23, 1),
	(24, 21, 26, 2);
/*!40000 ALTER TABLE `relation` ENABLE KEYS */;

-- Dumping structure for table fabrics.sewing
DROP TABLE IF EXISTS `sewing`;
CREATE TABLE IF NOT EXISTS `sewing` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `explanation` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table fabrics.sewing: ~7 rows (approximately)
/*!40000 ALTER TABLE `sewing` DISABLE KEYS */;
REPLACE INTO `sewing` (`Id`, `date`, `explanation`) VALUES
	(3, '2020-02-05', 'pluus'),
	(12, '2020-02-06', 'kikilips'),
	(19, '2020-02-05', 'püksid'),
	(21, '2020-02-10', 'lips'),
	(22, '2020-02-09', 'seelik'),
	(23, '2020-01-05', 'mantel'),
	(25, '2020-02-12', 'pluus');
/*!40000 ALTER TABLE `sewing` ENABLE KEYS */;

-- Dumping structure for table fabrics.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table fabrics.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `username`, `password`) VALUES
	(1, 'admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
