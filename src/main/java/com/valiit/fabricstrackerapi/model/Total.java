package com.valiit.fabricstrackerapi.model;

import java.time.LocalDate;

public class Total {
    private LocalDate startDate;
    private LocalDate endDate;
    private double fabricIn;
    private double moneyOut;
    private double fabricOut;
    private double costOfFabricOut;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getFabricIn() {
        return fabricIn;
    }

    public void setFabricIn(double fabricIn) {
        this.fabricIn = fabricIn;
    }

    public double getMoneyOut() {
        return moneyOut;
    }

    public void setMoneyOut(double moneyOut) {
        this.moneyOut = moneyOut;
    }

    public double getFabricOut() {
        return fabricOut;
    }

    public void setFabricOut(double fabricOut) {
        this.fabricOut = fabricOut;
    }

    public double getCostOfFabricOut() {
        return costOfFabricOut;
    }

    public void setCostOfFabricOut(double costOfFabricOut) {
        this.costOfFabricOut = costOfFabricOut;
    }

    @Override
    public String toString() {
        return "Total{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", fabricIn=" + fabricIn +
                ", moneyOut=" + moneyOut +
                ", fabricOut=" + fabricOut +
                ", costOfFabricOut=" + costOfFabricOut +
                '}';
    }
}
