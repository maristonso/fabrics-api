package com.valiit.fabricstrackerapi.model;

public class Relation {
    private Integer id;
    private Integer fabricId;
    private Integer sewingId;
    private double length;
    private Fabric fabric;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFabricId() {
        return fabricId;
    }

    public void setFabricId(Integer fabricId) {
        this.fabricId = fabricId;
    }

    public Integer getSewingId() {
        return sewingId;
    }

    public void setSewingId(Integer sewingId) {
        this.sewingId = sewingId;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Fabric getFabric() {
        return fabric;
    }

    public void setFabric(Fabric fabric) {
        this.fabric = fabric;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "fabricId=" + fabricId +
                ", sewingId=" + sewingId +
                ", length=" + length +
                '}';
    }
}
