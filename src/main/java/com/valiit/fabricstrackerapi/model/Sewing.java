package com.valiit.fabricstrackerapi.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Sewing {
    private Integer id;
    private LocalDate date;
    private String explanation;
    private List<Relation> relations = new ArrayList<>();
    private List<Fabric> fabrics = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    public List<Fabric> getFabrics() {
        return fabrics;
    }

    public void setFabrics(List<Fabric> fabrics) {
        this.fabrics = fabrics;
    }

    @Override
    public String toString() {
        return "Sewing{" +
                "id=" + id +
                ", date=" + date +
                ", explanation='" + explanation + '\'' +
                '}';
    }
}
