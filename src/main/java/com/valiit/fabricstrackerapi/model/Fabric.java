package com.valiit.fabricstrackerapi.model;

import java.time.LocalDate;

public class Fabric {
    private Integer id;
    private String weaveType;
    private String mainMaterial;
    private boolean washed;
    private String comment;
    private String composition;
    private String color;
    private String pattern;
    private double width;
    private double length;
    private double lengthRemained;
    private double weight;
    private double unitWeight;
    private double price;
    private double unitPrice;
    private LocalDate date;
    private String shop;
    private String url;
    private String purpose;

    public Fabric() {}

    public Fabric(String line){
        String[] data = line.split("\t");
        this.composition = data[6];
        switch(data[7]){
            case "jah": this.washed = true; break;
            case "Jah": this.washed = true; break;
            case "ei": this.washed = false; break;
            case "Ei": this.washed = false; break;
        }
        this.comment = data[5] + "; " + data[18];
        this.length = ExcelCellToFloat(data[8]);
        this.lengthRemained = ExcelCellToFloat(data[8]);
        this.width = ExcelCellToFloat(data[10]);
        this.weight = ExcelCellToFloat(data[11]);
        this.price = ExcelCellToFloat(data[14].replace("€",""));
        this.shop = data[17];

        String[] parts = data[1].replace(".",",").split(",");
        this.date = LocalDate.of(
                Integer.parseInt(parts[2]),
                Integer.parseInt(parts[1]),
                Integer.parseInt(parts[0]));
    }

    private float ExcelCellToFloat(String str){
        return str.equals("") ?  0F : Float.parseFloat(str.replace(",",".").trim());
    }




    public Integer getId() { return id; }

    public String getWeaveType() {
        return weaveType;
    }

    public void setWeaveType(String weaveType) {
        this.weaveType = weaveType;
    }

    public String getMainMaterial() {
        return mainMaterial;
    }

    public void setMainMaterial(String mainMaterial) {
        this.mainMaterial = mainMaterial;
    }

    public boolean getWashed() { return washed; }

    public void setWashed(boolean washed) { this.washed = washed;}

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getLengthRemained() { return lengthRemained; }

    public void setLengthRemained(double lengthRemained) { this.lengthRemained = lengthRemained; }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getUnitWeight() {
        return unitWeight;
    }

    public void setUnitWeight(double unitWeight) {
        this.unitWeight = unitWeight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public LocalDate getDate() { return date; }

    public void setDate(LocalDate date) { this.date = date; }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public String getPurpose() { return purpose; }

    public void setPurpose(String purpose) { this.purpose = purpose; }

    @Override
    public String toString() {
        return "Fabric{" +
                "id=" + id +
                ", weaveType='" + weaveType + '\'' +
                ", mainMaterial='" + mainMaterial + '\'' +
                ", isWashed=" + washed +
                ", comment='" + comment + '\'' +
                ", composition='" + composition + '\'' +
                ", color='" + color + '\'' +
                ", pattern='" + pattern + '\'' +
                ", width=" + width +
                ", length=" + length +
                ", lengthRemained=" + lengthRemained +
                ", weight=" + weight +
                ", unitWeight=" + unitWeight +
                ", price=" + price +
                ", unitPrice=" + unitPrice +
                ", date=" + date +
                ", shop='" + shop + '\'' +
                '}';
    }


}
