package com.valiit.fabricstrackerapi.service;

import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.model.Relation;
import com.valiit.fabricstrackerapi.model.Sewing;
import com.valiit.fabricstrackerapi.model.Total;
import com.valiit.fabricstrackerapi.repository.FabricRepository;
import com.valiit.fabricstrackerapi.repository.SewingRepository;
import com.valiit.fabricstrackerapi.repository.RelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TotalService {

    @Autowired
    private FabricRepository fabricRepository;
    @Autowired
    private SewingRepository sewingRepository;
    @Autowired
    private RelationRepository relationRepository;

    public Total getTotal(LocalDate startDate, LocalDate endDate){
        Total total = new Total();
        total.setStartDate(startDate);
        total.setEndDate(endDate);
        double totalLengthIn = 0;
        double totalMoneyOut = 0;
        double totalFabricOut = 0;
        double costOfFabricOut = 0;

        LocalDate date;
        List<Fabric> fabrics = fabricRepository.getFabrics();
        for(Fabric fabric:fabrics){
            date = fabric.getDate();
            if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0) {
                totalLengthIn = totalLengthIn + fabric.getLength();
                totalMoneyOut = totalMoneyOut + fabric.getPrice();
            }
        }
        total.setFabricIn(totalLengthIn);
        total.setMoneyOut(totalMoneyOut);

        Double unitPrice;
        Double len;
        List<Sewing> sewings = sewingRepository.getSewings();
        List<Relation> relations;
        for(Sewing sewing : sewings) {
            date = sewing.getDate();
            if(date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0) {
                relations = sewing.getRelations();
                for(Relation relation : relations){
                    len = relation.getLength();
                    totalFabricOut = totalFabricOut + len;
                    unitPrice = fabricRepository.getFabric(relation.getFabricId()).getUnitPrice();
                    costOfFabricOut = costOfFabricOut + len * unitPrice;
                }
            }

        }
        total.setFabricOut(totalFabricOut);
        total.setCostOfFabricOut(costOfFabricOut);
        System.out.println(total);
        return total;
    }
}
