package com.valiit.fabricstrackerapi.service;

import com.valiit.fabricstrackerapi.model.Sewing;
import com.valiit.fabricstrackerapi.repository.FabricRepository;
import com.valiit.fabricstrackerapi.repository.SewingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SewingService {

    @Autowired
    private SewingRepository sewingRepository;

    @Autowired
    private FabricRepository fabricRepository;

    public List<Sewing> getSewings() {
        return sewingRepository.getSewings();// pane tähele väikest algustähte!
    }

    public Sewing getSewing(int id){
        return sewingRepository.getSewing(id);
    }

    public void addOrUpdateSewing(Sewing sewing) {
        if(sewing.getId() != null) {
            //update sewing
            sewingRepository.updateSewing(sewing);
        } else {
            //add new sewing
            sewingRepository.addSewing(sewing);
            //muudab fabrics database
//            Integer fabricId = sewing.getFabricId();
//            Fabric fabric = fabricRepository.getFabric(fabricId);
//            Double usedLength = sewing.getLengthUsed();
//            Double oldLength = fabric.getLengthRemained();
//            Double newLength = oldLength-usedLength;
//            fabric.setLengthRemained(newLength);
//            fabricRepository.updateFabric(fabric);
        }
    }
    public void deleteSewing(int id) {
        sewingRepository.deleteSewing(id);
    }

}
