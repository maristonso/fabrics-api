package com.valiit.fabricstrackerapi.service;
import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.repository.FabricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.sort;

@Service
public class FabricService {

    @Autowired
    private FabricRepository fabricRepository;

    public List<Fabric> getFabrics() {
        List<Fabric> fabrics = fabricRepository.getFabrics();
        LocalDate kp = fabrics.get(0).getDate();
        Integer res = fabrics.get(0).getDate().compareTo(fabrics.get(1).getDate());
        //System.out.println("kuupäev on: "+kp );
        sort(fabrics, (a, b) -> a.getDate().compareTo(b.getDate()));
        return fabrics;
    }

    public Fabric getFabric(int id){
        return fabricRepository.getFabric(id);
    }

    public void addOrUpdateFabric(Fabric fabric) {

        if(fabric.getUnitWeight()==0) {
            double amount = fabric.getLength() * fabric.getWidth();
            if (amount > 0) {
                double uweight = fabric.getWeight() / amount;
                fabric.setUnitWeight(uweight);
            }
        }
        if(fabric.getUnitPrice()==0) {
            double uprice = fabric.getPrice()/fabric.getLength();
            fabric.setUnitPrice(uprice);
        }
        if(fabric.getId() != null){
            //update fabric
            fabricRepository.updateFabric(fabric);
        } else {
            //add new fabric
            fabric.setLengthRemained(fabric.getLength());
            System.out.println("Lisab uue fabricu ja väärtustab lengthRemained = length");
            fabricRepository.addFabric(fabric);
        }
    }

    public void deleteFabric(int id) {
        fabricRepository.deleteFabric(id);
    }



}
