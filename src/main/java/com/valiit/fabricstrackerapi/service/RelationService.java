package com.valiit.fabricstrackerapi.service;

import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.model.Relation;
import com.valiit.fabricstrackerapi.repository.FabricRepository;
import com.valiit.fabricstrackerapi.repository.RelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelationService {

    @Autowired
    private RelationRepository relationRepository;

    @Autowired
    private FabricRepository fabricRepository;


    public List<Relation> getRelations() {
        return relationRepository.getRelations();// pane tähele väikest algustähte!
    }

    public Relation getRelation(int id){
        return relationRepository.getRelation(id);
    }

    public void addOrUpdateRelation(Relation relation) {
        if(relation.getId() != null){
            //update fabric
            //relationRepository.updateRelation(relation);
        } else {
            //add new relation
            relationRepository.addRelation(relation);
            Fabric f = fabricRepository.getFabric(relation.getFabricId());
            Double newLength = f.getLengthRemained() - relation.getLength();
            f.setLengthRemained(newLength);
            fabricRepository.updateFabric(f);
        }
    }

    public void deleteRelation(int id) {
        Relation relation = getRelation(id);
        Fabric f = fabricRepository.getFabric(relation.getFabricId());
        Double newLength = f.getLengthRemained() + relation.getLength();
        f.setLengthRemained(newLength);
        fabricRepository.updateFabric(f);
        relationRepository.deleteRelation(id);
    }

}
