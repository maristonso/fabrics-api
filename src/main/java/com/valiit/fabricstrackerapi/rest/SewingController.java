package com.valiit.fabricstrackerapi.rest;

import com.valiit.fabricstrackerapi.model.Sewing;
import com.valiit.fabricstrackerapi.service.SewingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fabrics/sewings")
@CrossOrigin("*")
public class SewingController {


    @Autowired
    private SewingService sewingService;

    @GetMapping
    public List<Sewing> getSewings() {
        return sewingService.getSewings(); }

    @GetMapping("/{id}")
    public Sewing getSewing(@PathVariable("id") int id) {
        return sewingService.getSewing(id);
    }

    @PostMapping
    public void addOrUpdateSewing(@RequestBody Sewing sewing) { sewingService.addOrUpdateSewing(sewing); }


    @DeleteMapping("/{id}")
    public void deleteSewing(@PathVariable("id") int id) {
        sewingService.deleteSewing(id);
    }


}


