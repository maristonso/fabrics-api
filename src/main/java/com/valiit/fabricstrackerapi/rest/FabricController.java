package com.valiit.fabricstrackerapi.rest;



import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.service.FabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fabrics")
@CrossOrigin("*")
public class FabricController {

    @Autowired
    private FabricService fabricService;

    @GetMapping
    public List<Fabric> getFabrics() {
        return fabricService.getFabrics();
    }

    @GetMapping("/{id}")
    public Fabric getFabric(@PathVariable("id") int id) {
        return fabricService.getFabric(id);
    }


    @PostMapping
    public void addOrUpdateFabric(@RequestBody Fabric fabric) {
        fabricService.addOrUpdateFabric(fabric);
    }


    @DeleteMapping("/{id}")
    public void deleteFabric(@PathVariable("id") int id) {
        fabricService.deleteFabric(id);
    }




}

