package com.valiit.fabricstrackerapi.rest;

import com.valiit.fabricstrackerapi.model.Total;
import com.valiit.fabricstrackerapi.service.TotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/fabrics/totals")
@CrossOrigin("*")
public class TotalController {

    @Autowired
    private TotalService totalService;

    @PostMapping
    public Total getTotal(@RequestBody List<LocalDate> dates) {
        LocalDate startDate = dates.get(0);
        LocalDate endDate = dates.get(1);
        return totalService.getTotal(startDate, endDate);
    }
}
