package com.valiit.fabricstrackerapi.rest;
import com.valiit.fabricstrackerapi.model.Relation;
import com.valiit.fabricstrackerapi.service.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //defineerib kontrolleri klassi
@RequestMapping("/fabrics/relations")
@CrossOrigin("*")
public class RelationController {

    @Autowired
    private RelationService relationService;

    @GetMapping
    public List<Relation> getRelations() {
        return relationService.getRelations();
    }

    @GetMapping("/{id}")
    public Relation getRelation(@PathVariable("id") int id) {
        return relationService.getRelation(id);
    }


    @PostMapping
    public void addOrUpdateFabric(@RequestBody Relation relation) {
        relationService.addOrUpdateRelation(relation);
    }

    @DeleteMapping("/{id}")
    public void deleteRelation(@PathVariable("id") int id) {
        relationService.deleteRelation(id);
    }

}
