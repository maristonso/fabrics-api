package com.valiit.fabricstrackerapi.rest;

import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.service.FabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/fabrics/import")
@CrossOrigin("*")
public class ImportFabrics {

    @Autowired
    private FabricService fabricService;

    @GetMapping
    public void getFabrics() throws IOException {
        String tee = "C:\\uploads\\kangad.txt";

        List<String> stashLines = Files.readAllLines(Paths.get(tee), StandardCharsets.UTF_16);
        System.out.println(stashLines.get(1));

        List<Fabric> stash = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            Fabric newFabric = new Fabric(stashLines.get(i));
            fabricService.addOrUpdateFabric(newFabric);
            stash.add(newFabric);
            System.out.println(newFabric);
        }

    }
}

