package com.valiit.fabricstrackerapi.repository;

import com.valiit.fabricstrackerapi.model.Fabric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FabricRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Fabric> getFabrics() {
        return jdbcTemplate.query("select * from fabric", mapFabricRows);
    }

    public Fabric getFabric(int id){
        List<Fabric> fabrics = jdbcTemplate.query("select * from fabric where id = ?",
                new Object[]{id}, mapFabricRows);
        return fabrics.size() > 0 ? fabrics.get(0) : null;
    }

    public void addFabric(Fabric fabric) {
        jdbcTemplate.update(
                "insert into fabric (composition, " +
                        "Washed, mainMaterial, weaveType, comment," +
                        " color, pattern, " +
                        "width, length, lengthRemained, weight, " +
                        "unitWeight, price, unitPrice, date, shop, url, purpose) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                fabric.getComposition(), fabric.getWashed(), fabric.getMainMaterial(), fabric.getWeaveType(),
                fabric.getComment(), fabric.getColor(), fabric.getPattern(), fabric.getWidth(), fabric.getLength(),
                fabric.getLengthRemained(), fabric.getWeight(), fabric.getUnitWeight(), fabric.getPrice(),
                fabric.getUnitPrice(), fabric.getDate(), fabric.getShop(), fabric.getUrl(), fabric.getPurpose()
        );
    }



    public void updateFabric(Fabric fabric) {
        jdbcTemplate.update(
                "update fabric set `composition`=?, `color`=?,  `pattern`=?, " +
                        "`mainMaterial`=?, `weaveType`=?, `comment`=?, `washed`=?, " +
                        "`width`=?, `length`=?, `lengthRemained`=?, " +
                        "`weight`=?, `unitWeight`=?,`price`=?, " +
                        "`unitPrice`=?, `date`=?, `shop`=?, `url`=?, `purpose`=? where `id` =?",
                fabric.getComposition(), fabric.getColor(), fabric.getPattern(),

                fabric.getMainMaterial(), fabric.getWeaveType(), fabric.getComment(), fabric.getWashed(),

                fabric.getWidth(), fabric.getLength(), fabric.getLengthRemained(),
                fabric.getWeight(), fabric.getUnitWeight(), fabric.getPrice(),
                fabric.getUnitPrice(), fabric.getDate(), fabric.getShop(), fabric.getUrl(), fabric.getPurpose(), fabric.getId()
                );
    }

    public void deleteFabric(int id){
        jdbcTemplate.update("delete from fabric where `id`=?", id);
    }

    private RowMapper<Fabric> mapFabricRows = (rs, rowNum) -> {
        Fabric fabric = new Fabric();
        fabric.setId(rs.getInt("id"));
        fabric.setComposition(rs.getString("composition"));

        fabric.setWeaveType(rs.getString("weaveType"));
        fabric.setMainMaterial(rs.getString("mainMaterial"));
        fabric.setWashed(rs.getBoolean("washed"));
        fabric.setComment(rs.getString("comment"));


        fabric.setColor(rs.getString("color"));
        fabric.setPattern(rs.getString("pattern"));
        fabric.setWidth(rs.getDouble("width"));
        fabric.setLength(rs.getDouble("length"));
        fabric.setLengthRemained(rs.getDouble("lengthRemained"));

        fabric.setWeight(rs.getDouble("weight"));
        fabric.setUnitWeight(rs.getDouble("unitWeight"));
        fabric.setPrice(rs.getDouble("price"));
        fabric.setUnitPrice(rs.getDouble("unitPrice"));
        fabric.setDate(rs.getDate("date").toLocalDate());
        fabric.setShop(rs.getString("shop"));
        fabric.setUrl(rs.getString("url"));
        fabric.setPurpose(rs.getString("purpose"));

        return fabric;

    };
}
