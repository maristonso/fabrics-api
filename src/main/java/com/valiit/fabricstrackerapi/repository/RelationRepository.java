package com.valiit.fabricstrackerapi.repository;
import com.valiit.fabricstrackerapi.model.Fabric;
import com.valiit.fabricstrackerapi.model.Relation;
import com.valiit.fabricstrackerapi.service.FabricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RelationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private FabricService fabricService;

    public List<Relation> getRelations() {
        return jdbcTemplate.query("select * from relation", mapRelationRows);
    }



    public Relation getRelation(int id){
        List<Relation> relations = jdbcTemplate.query("select * from relation where id = ?",
                new Object[]{id}, mapRelationRows);
        return relations.size() > 0 ? relations.get(0) : null;
    }

    public List<Relation> getRelationsForSewing(int sewingId){
        List<Relation> relations = jdbcTemplate.query("select * from relation where sewingId = ?",
                new Object[]{sewingId}, mapRelationRows);
        return relations;
    }

    public List<Fabric> getFabricsForSewing(int sewingId){
        List<Relation> relations = getRelationsForSewing(sewingId);
        Integer fId;
        List<Fabric> fabricList = new ArrayList<>();
        for(int i=0; i<relations.size();i++) {
            fId = relations.get(i).getFabricId();
            fabricList.add(fabricService.getFabric(fId));
        }
        return fabricList;
    }


    public void addRelation(Relation relation) {
        jdbcTemplate.update(
                "insert into relation (fabricId, sewingId, length) values (?, ?, ?)",
                relation.getFabricId(), relation.getSewingId(), relation.getLength()
        );
    }

    public void updateRelation(Relation relation) {
        jdbcTemplate.update(
                "update sewing set `fabricId`=?, `sewingId`=?, `length`=? where id =?",
                relation.getFabricId(), relation.getSewingId(), relation.getLength(), relation.getId()
        );
    }

    public void deleteRelation(int id){
        jdbcTemplate.update("delete from relation where `id`=?", id);
    }


    private RowMapper<Relation> mapRelationRows = (rs, rowNum) -> {
        Relation relation = new Relation();
        relation.setId(rs.getInt("Id"));
        relation.setFabricId(rs.getInt("fabricId"));
        relation.setSewingId(rs.getInt("sewingId"));
        relation.setLength(rs.getDouble("length"));
        return relation;

    };

}
