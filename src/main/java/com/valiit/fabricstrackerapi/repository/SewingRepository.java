package com.valiit.fabricstrackerapi.repository;

import com.valiit.fabricstrackerapi.model.Sewing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SewingRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RelationRepository relationRepository;

    public List<Sewing> getSewings() {
        return jdbcTemplate.query("select * from sewing", mapSewingRows);
    }

    public Sewing getSewing(int id){
        List<Sewing> sewings = jdbcTemplate.query("select * from sewing where id = ?",
                new Object[]{id}, mapSewingRows);
        return sewings.size() > 0 ? sewings.get(0) : null;
    }

    public void addSewing(Sewing sewing) {
        jdbcTemplate.update(
                "insert into sewing (date, explanation) values (?, ?)",
                sewing.getDate(), sewing.getExplanation()
        );
    }

    public void updateSewing(Sewing sewing) {
        jdbcTemplate.update(
                "update sewing set `date`=?, `explanation`=? where id =?",
                sewing.getDate(), sewing.getExplanation(), sewing.getId()
        );
    }

    public void deleteSewing(int id){
        jdbcTemplate.update("delete from sewing where `id`=?", id);
    }




    private RowMapper<Sewing> mapSewingRows = (rs, rowNum) -> {
        Sewing sewing = new Sewing();
        sewing.setId(rs.getInt("Id"));
        sewing.setDate(rs.getDate("date").toLocalDate());
        sewing.setExplanation(rs.getString("explanation"));
        sewing.setRelations(relationRepository.getRelationsForSewing(rs.getInt("Id")));
        sewing.setFabrics(relationRepository.getFabricsForSewing(rs.getInt("Id")));
        return sewing;

    };
}
