package com.valiit.fabricstrackerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabricsTrackerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabricsTrackerApiApplication.class, args);
	}
}
