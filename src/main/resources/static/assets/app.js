let currentActiveFabricId = 0;
let currentActiveSewingId = 0;
let fabrics = [];
let sewings = [];
// globaalse muutuja defineerimine enter-iga salvestamise jaoks
let currentlyOpenPopup = null;

let materials =  [ 
    ["Undefined", "Määramata" ],
    ["Wool", "Vill"],
    ["Cot", "Puuvill"],
    ["Visc", "Viskoos"],
    ["Silk", "Siid"],
    ["Poly", "Polüester"],
    ["Linen", "Linane"]
]

let weaves = [
    ["Undetermined", "Määramata"],
    ["Plain", "Labane"],
    ["Jeans", "Teksa"],
    ["Gabardine", "Gabardiin"],
    ["Twill", "Tvill"],
    ["Jersey", "Trikotaaž"],
    ["Sweat", "Dressiriie"],
    ["Ponte", "Ponte"],
    ["Velvet", "Samet"],
    ["Corduroy", "Velvet (pesusamet)"]
]

let purposes = [
    ["Undefined", "Määramata"],
    ["Shirting", "Pluusid, särgid"],
    ["Trousers", "Püksid"],
    ["Jackets", "Pintsakud, jakid"],
    ["Dresses", "Kleidid"],
    ["Lining", "Vooder"],
    ["Pocketing", "Taskuriie"],
    ["Furniture", "Mööbliriie"],
    ["Home", "Kodumajapidamine"]
]

 
// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {
    createPropertyCheckBoxes('material', materials);
    createPropertyCheckBoxes('weave', weaves);
    createPropertyCheckBoxes('purpose', purposes);
    doLoadFabrics();
    doLoadSewings();
});


/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadFabrics() {    /* jääb ootama kuni andmed tulevad */
    fabrics = await getFabrics();
    displayFabrics(fabrics);
}

async function doLoadSewings() {
    sewings = await getSewings();
    displaySewings(sewings);
}


async function doDeleteFabric(id) {
    let allowDelete = true;
    for(let i = 0; i < sewings.length; i++) 
        for(let j = 0; j < sewings[i].relations.length; j++)
            if(sewings[i].relations[j].fabricId == id){
                allowDelete = false;
            }
    if(allowDelete) {
        if (confirm('Oled kindel, et tahad seda kangast kustutada?')) {
            await deleteFabric(id);
            doLoadFabrics();
        }
    } else {
        alert("Enne kanga kustutamist tuleb see eemaldada õmblustöö(de) juurest!")
    }
}

async function doDeleteSewing(id) {
    if (confirm('Oled kindel, et tahad seda tööd kustutada?')) {
        await deleteSewing(id);
        doLoadSewings();
    }
}

async function doDeleteFabricRelation(relationId) {
    if (confirm('Oled kindel, et tahad selle kanga selle õmblustöö juurest eemaldada?')) {
        await deleteRelation(relationId);
        doLoadSewings();
        doLoadFabrics();
    }
}



async function doSaveFabric() {
    let myFabric = {
        mainMaterial: document.querySelector('#fabricMainMaterialInput').value,
        composition: document.querySelector('#fabricCompositionInput').value,
        weaveType: document.querySelector('#fabricWeaveTypeInput').value,
        purpose: document.querySelector('#fabricPurposeInput').value,
        url: document.querySelector('#itemEditFabric').value,
        color: document.querySelector('#fabricColorInput').value,
        pattern: document.querySelector('#fabricPatternInput').value,
        length: document.querySelector('#fabricLengthInput').value,
        lengthRemained: document.querySelector('#fabricLengthRemainedInput').value,
        width: document.querySelector('#fabricWidthInput').value,
        weight: document.querySelector('#fabricWeightInput').value,
        price: document.querySelector('#fabricPriceInput').value,
        isWashed: document.querySelector('#fabricIsWashedInput').checked,
        date: document.querySelector('#fabricDateInput').value,
        shop: document.querySelector('#fabricShopInput').value,
        comment: document.querySelector('#fabricCommentInput').value,
    };
    if (currentActiveFabricId > 0) {
        myFabric.id = currentActiveFabricId;
    }
    await saveFabric(myFabric);
    await doLoadFabrics();
    await doLoadSewings();
    closePopup();
}



async function doSaveSewing() {
    let mySewing = {
        date: document.querySelector('#sewingDateInput').value,
        explanation: document.querySelector('#sewingExplanationInput').value,
    };
    if (currentActiveSewingId > 0) {
        mySewing.id = currentActiveSewingId;
    }
    await saveSewing(mySewing);
    await doLoadSewings();
    await doLoadFabrics();
    closePopup();
}


async function doSaveSewingFabric() {
    let e = document.getElementById("fabricsMenu");
    let mySewingFabric = {
        length: parseFloat(document.querySelector('#sewingFabricLengthInput').value),
        fabricId: parseInt(e.options[e.selectedIndex].value),
        sewingId: currentActiveSewingId
    }
    let i = 0;
    while(fabrics[i].id !== mySewingFabric.fabricId){
        i++;
    }
    if(fabrics[i].lengthRemained-mySewingFabric.length<0){
        alert("Seda kangast ei ole nii palju, sisesta väiksem pikkus!")
    } else {
        await saveSewingFabric(mySewingFabric);
        await doLoadSewings();
        await doLoadFabrics();
        closePopup();
    }
}

async function  doUploadFile() {
    const fabricFile = document.querySelector('#file').files[0];
    if (fabricFile) {
        let uploadResponse = await postFile(fabricFile);
        console.log("Upload Response:");
        console.log(uploadResponse);
        document.querySelector('#itemEditFabricUrl').src = uploadResponse.url;
        document.querySelector('#itemEditFabric').value = uploadResponse.url;
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function FabricRowHTML(fabric, bgColor) {
    return /*html*/`
    <tr style="background-color:${bgColor}">
       <td>${fabric.composition}</td>
       <td class="narrow-table-cell">${fabric.mainMaterial}</td>
       <td>${fabric.weaveType}</td>
       <td>${fabric.color}</td>
       <td>${fabric.pattern}</td>
       <td><img src=${fabric.url}></td>
       <td class="narrow-table-cell">${fabric.length.toFixed(2)}</td>
       <td class="narrow-table-cell">${fabric.lengthRemained.toFixed(2)}</td>
       <td class="narrow-table-cell">${fabric.width.toFixed(2)}</td>
       <td>${fabric.unitWeight.toFixed(2)}</td>
       <td>${fabric.price.toFixed(2)}</td>
       <td>${fabric.unitPrice.toFixed(2)}</td>
       <td>${fabric.date}</td>
       <td>${fabric.shop}</td>
       <td>${fabric.comment}</td>
       <td>
          <button class="button-red" onclick="doDeleteFabric(${fabric.id})">Kustuta</button>
          <button class="button-blue" onclick="displayFabricEditPopup(${fabric.id})">Muuda</button>
         
       </td>
   </tr>`
}

function displayFabrics(fabrics) { 
    let fabricsHTML = '';
    for (let i = 0; i < fabrics.length; i++) {
        let bg = (fabrics[i].lengthRemained < 0.5) ? "LightSlateGrey" : "FloralWhite";
        let lengthCond = document.getElementById("all-lengths").checked || fabrics[i].lengthRemained > 0.5;
        let materialCheckBox = document.getElementById(`material-${fabrics[i].mainMaterial}`);
        let weaveCheckBox = document.getElementById(`weave-${fabrics[i].weaveType}`);
        if(lengthCond && materialCheckBox.checked && weaveCheckBox.checked){
            fabricsHTML = fabricsHTML + FabricRowHTML(fabrics[i], bg);
        }
    }
    document.querySelector('#fabricsListRows').innerHTML = fabricsHTML;
}

function displaySewings(sewings) {
    let rownum;
    let sewingsHTML = '';
    for (let i = 0; i < sewings.length; i++) {
        let fabricsHTML ='';
        if(sewings[i].fabrics.length === 0) {
            fabricsHTML = '<td class="wide-table-cell"></td><td></td>'
            rownum = 1;
        } else {
            rownum = sewings[i].fabrics.length;
            for (let j = 0; j < sewings[i].fabrics.length; j++) {
                fabricsHTML = fabricsHTML + 
                `<tr><td class="wide-table-cell"> 
                    ${sewings[i].fabrics[j].color}, 
                    ${sewings[i].fabrics[j].pattern}, 
                    ${sewings[i].fabrics[j].composition}, 
                    ${sewings[i].relations[j].length} m
                </td><td>
                    <button onclick="doDeleteFabricRelation(${sewings[i].relations[j].id})">Kustuta</button>
                </td>
                </tr>`
            }
            fabricsHTML = fabricsHTML.substr(4,(fabricsHTML.length)-9);       
        } 

        sewingsHTML = sewingsHTML + /*html*/`
            <tr>
                <td rowspan="${rownum}">${sewings[i].id}</td>
                <td rowspan="${rownum}">${sewings[i].date}</td>
                <td rowspan="${rownum}">${sewings[i].explanation}</td>
                <td rowspan="${rownum}" class = "wide-table-cell">
                    <button class="button-red" onclick="doDeleteSewing(${sewings[i].id})">Kustuta</button>
                    <button class="button-blue" onclick="displaySewingEditPopup(${sewings[i].id})">Muuda</button>
                    <button class="button-blue" onclick="displaySewingFabricAddPopup(${sewings[i].id})">Lisa kangas</button>
               </td>
               ${fabricsHTML}
            </tr>`;
    }
    document.querySelector('#sewingsListRows').innerHTML = sewingsHTML;
}


async function displayFabricEditPopup(id) {
    // Enter klahviga salvestamise jaoks currentlyOpenPopup
    currentlyOpenPopup = "fabricEditFormContainer";
    await openPopup(POPUP_CONF_LARGE, 'fabricEditFormTemplate');
    await populatePropertyDropDown('#fabricMainMaterialInput', materials);//see peab olema pärast popup'i avamist
    await populatePropertyDropDown('#fabricWeaveTypeInput', weaves);
    await populatePropertyDropDown('#fabricPurposeInput', purposes);

    if (id > 0) {
        //muutmine
        let fabric = await getFabric(id);
        document.querySelector('#fabricMainMaterialInput').value = fabric.mainMaterial;
        document.querySelector('#fabricCompositionInput').value = fabric.composition;
        document.querySelector('#fabricWeaveTypeInput').value = fabric.weaveType;
        document.querySelector('#fabricPurposeInput').value = fabric.purpose;
        document.querySelector('#fabricColorInput').value = fabric.color;
        document.querySelector('#fabricPatternInput').value = fabric.pattern;
        document.querySelector('#itemEditFabric').value = fabric.url;
        document.querySelector('#fabricLengthInput').value = fabric.length.toFixed(2);
        document.querySelector('#fabricLengthRemainedInput').value = fabric.lengthRemained.toFixed(2);
        document.querySelector('#fabricWidthInput').value = fabric.width.toFixed(2);
        document.querySelector('#fabricWeightInput').value = fabric.weight.toFixed(2);
        document.querySelector('#fabricPriceInput').value = fabric.price.toFixed(2);
        document.querySelector('#fabricIsWashedInput').checked = fabric.isWashed;
        document.querySelector('#fabricDateInput').value = fabric.date;
        document.querySelector('#fabricShopInput').value = fabric.shop;
        document.querySelector('#fabricCommentInput').value = fabric.comment;
        currentActiveFabricId = fabric.id; //globaalne muutuja
    } else {
        //lisamine
        currentActiveFabricId = 0;
        document.querySelector('#fabricDateInput').valueAsDate = new Date();
        document.querySelector('#fabricWidthInput').value = '1.5';
    }

}

async function displaySewingEditPopup(id) {
     // Enter klahviga salvestamise jaoks currentlyOpenPopup
    currentlyOpenPopup = "sewingEditFormContainer";
    await openPopup(POPUP_CONF_DEFAULT, 'sewingEditFormTemplate');

    if (id > 0) {
        //muutmine
        let sewing = await getSewing(id);
        document.querySelector('#sewingDateInput').value = sewing.date;
        document.querySelector('#sewingExplanationInput').value = sewing.explanation;

        currentActiveSewingId = sewing.id; //globaalne muutuja
    } else {
        //lisamine
        currentActiveSewingId = 0;
    }

}

async function displaySewingFabricAddPopup(id) {
    // Enter klahviga salvestamise jaoks currentlyOpenPopup
    currentlyOpenPopup = "SewingFabricAddFormContainer";
    await openPopup(POPUP_CONF_DEFAULT, 'SewingFabricAddFormTemplate');

    let fabricsMenuHTML = '';
    for (let i = 0; i < fabrics.length; i++) {
        fabricsMenuHTML = fabricsMenuHTML + /*html*/`
            <option value="${fabrics[i].id}">${fabrics[i].color}, ${fabrics[i].composition},  (${fabrics[i].lengthRemained}, m)</option>`;
    }
    document.querySelector('#fabricsMenu').innerHTML = fabricsMenuHTML;
    currentActiveSewingId = id;
}

async function displayTotals(){
    startDate = document.querySelector('#statisticsStartDateInput').value;
    endDate = document.querySelector('#statisticsEndDateInput').value;
    let totalsData = await getTotals(startDate, endDate);
    document.querySelector('#totalFabricIn').innerHTML = `${totalsData.fabricIn} m`;
    document.querySelector('#totalFabricOut').innerHTML = `${totalsData.fabricOut} m`;
    document.querySelector('#totalMoneyOut').innerHTML = `${totalsData.moneyOut.toFixed(2)} €`;
    document.querySelector('#totalCostOfFabricOut').innerHTML = `${totalsData.costOfFabricOut.toFixed(2)} €`;
}


// ===== show/hide fabrics: checkboxes and radios


//calling: selectAllCheckBoxes('weave')
async function selectAllCheckBoxes(prop){ //jep, see peab olema async ja väikese tähega
    let ticked = document.getElementById(`${prop}-all`).checked;
    let checkBoxes = document.getElementsByName(`${prop}-check-box`); 
    checkBoxes.forEach(box => {box.checked = ticked;});
    doLoadFabrics();
}

//calling: ('weave', weaves)
async function createPropertyCheckBoxes(prop, propValues){
    let html = `<input 
        type = "checkbox" 
        id = "${prop}-all" 
        onclick = "selectAllCheckBoxes('${prop}')" checked> 
    <label 
        for="${prop}-all">
        Vali/eemalda kõik
    </label><br>`;
    for(let i = 0; i<propValues.length; i++){
        let row = `<input 
            type = "checkbox" 
            name = "${prop}-check-box" 
            id = "${prop}-${propValues[i][0]}" 
            onclick = "doLoadFabrics()" checked>
        <label 
            for = "${prop}-${propValues[i][0]}" >
            ${propValues[i][1]}
        </label><br>`
        html = html + row;
    }
    id = `#${prop}CheckBoxes`;
    document.querySelector(id).innerHTML = html;
}


// calling: populatePropertyDropDown('#fabricWeaveTypeInput', weaves)
async function populatePropertyDropDown(id, propValues ){ 
    html = '';
    for(let i = 0; i<propValues.length; i++){
         let item = `<option value=${propValues[i][0]}>${propValues[i][1]}</option>`;
         html = html + item;
    }
    document.querySelector(id).innerHTML = html;
}

