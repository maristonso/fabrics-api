// suhtlus serveriga


//fabrics

async function getFabrics() {
    try {
        let response = await fetch(API_URL + '/fabrics');
        let fabrics = await response.json();
        return fabrics;
    } catch (e) {
        console.log('kangaste andmebaasi laadimine ebaõnnestus', e);
    }
}

async function getFabric(id) {
    try {
        let response = await fetch(API_URL + '/fabrics/' + id);
        let fabric = await response.json();
        return fabric;
    } catch (e) {
        console.log("Kanga detailide laadimine ebaõnnestus", e);
    }
}

async function deleteFabric(id) {
    try {
        let response = await fetch(API_URL + '/fabrics/' + id, { method: 'DELETE' });
    } catch (e) {
        console.log('ei saa kustutada millegipärast', e);
    }
}

async function saveFabric(fabric) {
    try {
        let requestUrl = API_URL + '/fabrics';
        let conf = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(fabric)
        }
        let result = await fetch(requestUrl, conf);
    } catch (e) {
        console.log('Viga salvestamisel...')
    }
}

// sewings

async function getSewings() {
    try {
        let response = await fetch(API_URL + '/fabrics/sewings');
        let sewings = await response.json();
        return sewings;
    } catch (e) {
        console.log('õmblustööde andmebaasi laadimine ebaõnnestus', e);
    }
}



async function getSewing(id) {
    try {
        let response = await fetch(API_URL + '/fabrics/sewings/' + id);
        let sewing = await response.json();
        return sewing;
    } catch (e) {
        console.log("Õmblustöö detailide laadimine ebaõnnestus", e);
    }
}

async function deleteSewing(id) {
    try {
        let response = await fetch(API_URL + '/fabrics/sewings/' + id, { method: 'DELETE' });
    } catch (e) {
        console.log('ei saa kustutada millegipärast', e);
    }
}

async function saveSewing(sewing) {
    try {
        let requestUrl = API_URL + '/fabrics/sewings';
        let conf = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(sewing)
        }
        let result = await fetch(requestUrl, conf);
    } catch (e) {
        console.log('Viga salvestamisel...')
    }
}

//sewingFabrics

async function getSewingFabrics() {
    try {
        let response = await fetch(API_URL + '/fabrics/relations');
        let sewingFabrics = await response.json();
        return sewingFabrics;
    } catch (e) {
        console.log("Õmblusprojektide ja kangaste seosetabeli laadimine ebaõnnestus", e);
    }
}

async function saveSewingFabric(sewingFabric) {
    try {
        let requestUrl = API_URL + '/fabrics/relations';
        let conf = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(sewingFabric)
        }
        let result = await fetch(requestUrl, conf);
    } catch (e) {
        console.log('Viga salvestamisel...')
    }
}

async function deleteRelation(id) {
    try {
        let response = await fetch(API_URL + '/fabrics/relations/' + id, { method: 'DELETE' });
    } catch (e) {
        console.log('ei saa kustutada millegipärast', e);
    }
}

async function postFile(file) {
    console.log(file);
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            body: formData
        }
    );
    return await response.json();
}

async function getTotals(startDate, endDate){
    try{
        let dates = [startDate, endDate];
        let requestUrl = API_URL + '/fabrics/totals';
                let conf = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(dates)
            }
        let result = await fetch(requestUrl, conf);
        // console.log(JSON.stringify(dates));
        // console.log('fabric in-out:');
        // console.log(result);
        return await result.json();
    } catch (e) {
        console.log('Ei saa kogusummat laadida');
    }

}
